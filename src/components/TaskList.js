const React = require('react');
const _ = require('lodash');

/**
 * The TaskList component renders a view for a list
 * of tasks.
 */
const TaskList = React.createClass({
  // Display name for the component (useful for debugging)
  displayName: 'TaskList',

  getInitialState: function() {
    return {name: 'boo'};
  },

  // Describe how to render the component
  render: function() {
    const arrayOfTaskComponents = _.map(this.props.myTasks, task => <Task key={task.id} {...task} />);
    const onNameChange = (event) => {this.setState({name: event.target.value})};
    const onButtonClick = (event) => {
        this.props.showMessage("Hello, " + this.state.name + "!");
      };

    return (
      <div>
        <ul>
        <input type="text" value={this.state.name} onChange={onNameChange}/>
          {arrayOfTaskComponents}
        </ul>
        <button type="button" onClick={onButtonClick} >Click here</button>
      </div>
    );
  }
});

const Task = React.createClass({
  displayName: 'Task',
    getDefaultProps: function() {
    return {
      description: 'This is the default Description'
    };
    },
  render: function(){
    return (
      <div>
      <li>{ this.props.description }</li>
      <input type="checkbox" checked={this.props.completed} readOnly />
      </div>
    );
  }
});


// Export the TaskList component
module.exports = TaskList;
