const React = require('react');

// Require our TaskList React component
const TaskList = require('./TaskList');

/**
 * The root React component from which all other components
 * on the page are descended.
 */

    const tasks = [
 {id: 1, description: "Clean my bed", completed: false},
 {id: 2, description: "Finish my homework", completed: true},
 {id: 3, description: "Brush my teeth", completed: false}
];

const Root = React.createClass({
  // Display name for the component (useful for debugging)
  displayName: 'Root',

  // Describe how to render the component
  render: function() {

    return (
      <TaskList myTasks = {tasks} showMessage={(msg) => alert(msg)}/>
    );
  }
});

// Export the Root component
module.exports = Root;
